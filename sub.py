import codecs
from datetime import date, datetime, timedelta
import logging
import math
import re
import speech_recognition as sr
from pydub import AudioSegment
import os


logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

max_duration = timedelta(seconds=15).total_seconds() * 1000
base_path = 'C:\\Users\\Eu\\Desktop\\'

def create_sub():
    input_file, output_file, temp_file = get_file_name()
    
    logging.info('Input file: %s', input_file)
    logging.info('Temp file: %s', temp_file)
    logging.info('Output file: %s', output_file)

    # Load the video file
    audio = get_video_audio(input_file)  
    num_parts, audio_parts = split_video_audio(audio)
    
    # Initialize recognizer class (for recognizing the speech)
    text = transcript_video_audio(num_parts, audio_parts)
    
    logging.info('Text extracted')

    create_transcription_file(temp_file, text)
    logging.info('Temp file created')
    
    convert_to_utf8(output_file, temp_file)
    logging.info('Converted transcription file from ANSI to UTF-8')

    os.remove(temp_file)    
    logging.info('Temp transcription file deleted')

    remove_wav_files()
    logging.info('Temp wav files deleted')
    logging.info('Process completed')

def get_file_name():
    base_date = date(2023, 7, 28)
    today = date.today()

    days = (today - base_date).days + 1

    if datetime.now().hour <= 12:
        days -= 1

    logging.info("Assuming day %s", days)
    
    file_base = f'dia {days}'

    input_file = f'{base_path}{file_base}.mp4'
    output_file = f'{base_path}{file_base}.txt'
    temp_file = f'{base_path}transcription.txt'

    return input_file, output_file, temp_file

def create_transcription_file(temp_file, text):
    with open(temp_file, "w") as file:
        file.write(text)
    
def convert_to_utf8(output_file, temp_file):
    blockSize = 1048576
    with codecs.open(temp_file,"r",encoding="mbcs") as sourceFileContent:
        with codecs.open(output_file,"w",encoding="UTF-8") as targetFile:
            while True:
                contents = sourceFileContent.read(blockSize)
                if not contents:
                    break
                targetFile.write(contents)

def transcript_video_audio(num_parts, audio_parts):
    r = sr.Recognizer()
    
    # Open the audio file
    text_parts = []
    for i, part in enumerate(audio_parts):
        file_name = f"part_{i}.wav"        
        part.export(file_name, format="wav")
        with sr.AudioFile(file_name) as source:
            audio_text = r.record(source)
            # Recognize the speech in the audio
            logging.info("Calling recognition for part %s of %s", (i+1), num_parts)
            text_parts.append(r.recognize_google(audio_text, language='pt-BR'))
    
    return ' '.join(text_parts)

def get_video_audio(input_file):
    video = AudioSegment.from_file(input_file, format="mp4")
    audio = video.set_channels(1).set_frame_rate(16000).set_sample_width(2)
    return audio

def split_video_audio(audio):
    num_parts = math.ceil(len(audio) / max_duration)
    logging.info('Audio to be splited in %s parts', num_parts)
    audio_parts = [audio[i * max_duration : (i + 1) * max_duration] for i in range(num_parts)]
    return num_parts,audio_parts

def remove_wav_files():
    logging.info('Removing wav files')
    [os.remove(f) for f in os.listdir('.') if re.match(r'part_[0-9]+.*\.wav', f)]
    logging.info('All wav files removed')


if __name__ == "__main__":
    create_sub()
